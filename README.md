Version 2.0.0

Instead of performing calculations on a video stream, a picture is taken at the beginning and is then used. This consumes less computing power and allows to fully exploit the Raspberry Pi resolution (max: 3280x2464).

Current lighting: ring

Backlight currently not on as it causes reflection of the ring light. Idea: place a sheet of paper above the backlight? Would still allow light to pass while not reflecting the one originating from the ring.

Current limitations: contour area detected is apparently not accurate enough to distinguish and exclusively select vertically placed screws, also detects some that are slightly horizontal.

Approaches to test (not mutually exclusive! Implement both):
- better contour area calculations
- templeate matching or deep learning in order to evaluate contour areas that come in question and label as screw in correct position/orientation or not.
