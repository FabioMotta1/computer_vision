'''
Basic application to take videos with Raspberry Pi and print some basic info
'''

import numpy as np
import cv2

cap=cv2.VideoCapture(0)
# get camera resolution: (standard = 640 x 480)
w = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
h = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
print(w, h)
# here you can set the camera resolution (standard = 640 x 480)
#cap.set(3, 640)  # width
#cap.set(4, 480)  # height

while(1):
	_,frame=cap.read()
	cv2.imshow('Video Stream', frame)
	k=cv2.waitKey(5)
	if k==27:
		break

cv2.destroyAllWindows()

print(frame)
print(frame.shape)
