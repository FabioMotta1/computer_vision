import numpy as np
import io
import cv2
import sys
import picamera

'''
Detect torx screws on Vereinzelungsplatte (Vplatte) even if in different positions
'''

#Create a memory stream so photos doesn't need to be saved in a file
stream = io.BytesIO()
'''
# get camera resolution: (standard = 640 x 480)
w = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
h = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
print(w, h)
'''
# accepted resolutions (4:3): 3280x2464, 1640x1232
# Parameters
res = input("Choose camera resolution from the following: \n 1) 1920x1080 \n 2) 3280x2464 \n")
threshold_value = 105 # threshold value between 100 and 110 works well with daylight, 90 in lowlight condition
min_area = 2000 # minimum area of object in order to be highlighted / detected (250 night, 300/350 daylight)
max_area = 3000
# for coordinates calculation2
size_of_image= 26.3 # width in cm of the field of view
if res == str(1):
    # set camera resolution (standard = 640 x 480)
    cam_x = 1920  # width
    cam_y = 1080  # height
    print("Selected resolution is 1920x1080")
    # for coordinates calculation
    cm_to_pixel = size_of_image/1920.0
    # cut region of the FOW (=Resion Of Interest - ROI)
    # vplatte location for (1280 x 720)
    y1 = 295
    y2 = 900
    x1 = 540
    x2 = 1175
    
elif res == str(2):
    cam_x = 3280  # width
    cam_y = 2464  # height
    print("Selected resolution is 3280x2464")
    # for coordinates calculation
    cm_to_pixel = size_of_image/3280.0
    # cut region of the FOW (=Resion Of Interest - ROI)
    # vplatte location for (1280 x 720)
    y1 = 670
    y2 = 1600
    x1 = 1125
    x2 = 2080
    
else:
    print("Please only choose one of the given alternatives")
    sys.exit()
    
    
with picamera.PiCamera() as camera:
    camera.resolution = (cam_x, cam_y)
    camera.capture(stream, format='jpeg')
    
#Convert the picture into a numpy array
buff = np.frombuffer(stream.getvalue(), dtype=np.uint8)

#Now creates an OpenCV image
image = cv2.imdecode(buff, 1)
cv2.imwrite('full_image.jpg',image)
'''
"frame" gets the next frame in the camera (via "cap").
"_" stands for the boolean variable "Ret" of cap.read()that obtains a return value from getting the camera
frame; either true (if the frame is available) or false.
Here we don't need this varible, so we can ignore it by using the char "_".
Documentation: https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html#bool%20VideoCapture::grab()
cap.read() is combination of cap.grab() and cap.retrieve().
From documentation: "This is the most convenient method for reading video files or capturing data from decode and return the just grabbed frame.
If no frames has been grabbed (camera has been disconnected, or there are no more frames in video file),
the methods return false and the functions return NULL pointer."
'''
# cut region of the FOW (=Resion Of Interest - ROI)
vplatte = image[y1: y2, x1: x2] # rows and then cols! Set above according to resolution
cv2.imwrite('vplatte.jpg',vplatte)
vplatte_gray = cv2.cvtColor(vplatte, cv2.COLOR_BGR2GRAY)
'''
find way to achieve optimal threshold value (value between 100 and 110 works well).
[retval, dst=cv2.threshold(src, thresh, maxval, type[, dst]) read documentation for further info]
convert all pixels to either white or black and then inverse the values (black bgd and objects in white)
'''
_, threshold = cv2.threshold(vplatte_gray, threshold_value, 255, cv2.THRESH_BINARY_INV)
cv2.imwrite('threshold.jpg',threshold)
    # Detect the torx screws
contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
area_values = []
n_objects = 0
final_coordinates = []
for cnt in contours:
    (x, y, w, h) = cv2.boundingRect(cnt)
    # highlight all detected contours with rectangle:
    #cv2.rectangle(vplatte, (x, y), (x + w, y + h), (0, 255, 0), 3) #(0, 255, 0) is green
    # Calculate area of contours in order to differenciate them & ignore the noise (e.g. holes in the Vplatte)
    area = cv2.contourArea(cnt)

    # Distinguish torx screws
    if area >= min_area and area <= max_area: # 250 lowlight, 350 with better lighting
        cv2.putText(vplatte, str(area), (x, y), 1, 1, (255, 0, 0))
        area_values.append(area)
        n_objects += 1
        # calculate coordinates of object
        # min enclosing circle
        (x,y),radius = cv2.minEnclosingCircle(cnt)
        center = (int(x),int(y))
        radius = int(radius)
        cv2.circle(vplatte,center,radius,(0,0,255),2)
        '''
To simplify calculations and detection, the FOW is cut to only the vplatte. Hence, the detected objects are in the
vplatte coordinate system; point (0,0) is upper left corner of the vplatte, not of the whole FOW.
In order to find the actual coordinates in respect to the FOW (in pixels) we need to add the portion of FOW around
the vplatte that was removed (section "cut region of the FOW" in resolution selection switch)
         '''
        # adjust coordinates from vplatte to frame (full FOW)
        x = x + x1
        y = y + y1
        #print('coordinates: ', x, ', ', y)
        x_cm = x * cm_to_pixel
        y_cm = y * cm_to_pixel
        coord = (x_cm, y_cm)
        final_coordinates.append(coord)
        #print('cm coordinates: ', x_cm, ', ', y_cm)
        #if n_objects == 8:
         #   print(final_coordinates)
          #  sys.exit()

#print(area_values)

while True:
    cv2.namedWindow('Frame', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Frame', 1000, 800)
    cv2.namedWindow('Vplatte', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Vplatte', 1000, 1000)
    cv2.namedWindow('Threshold', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Threshold', 1000, 1000)

    cv2.imshow("Frame", image)
    cv2.imshow("Vplatte", vplatte)
    cv2.imshow("Threshold", threshold)
    key = cv2.waitKey(1)
    if key == 27: #esc key
        #print('Found ', n_objects, 'objects!')
        break

cv2.destroyAllWindows()
